# SalesCandy Graph API Sample Application

This collection of scripts illustrates scenarios using the SalesCandy Graph API

- [Winning a lead](./winning-a-lead)


## Graph API Documentation

Refer to the documentation at

1. https://docs.graph.salescandy.com
2. https://support.salescandy.com/hc/en-us/articles/360039313872-SalesCandy-Graph-API

## Contact Support

For any assistance, please contact our Customer Success Team at support@salescandy.com
