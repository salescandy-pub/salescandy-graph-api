import 'dotenv/config';
import fetch from "node-fetch";
import * as utils from "./utils.js";

const API_URL = process.env.API_URL ?? 'https://graph.v3.salescandy.com';
const JWT = process.env.JWT;

/**
 * Fetches a list of leads
 *
 * @param where object  filter conditions
 * @returns {Promise<*[]>}
 */
export async function getLeadList(where = {}) {

    const query = `query GetLeadList(
                            $where:GetLeadListInput!
                        ) { 
                        getLeadList(where:$where) { 
                            token
                            total
                            resultCount
                            filteredCount
                            data {leadId}
                            dataGzip
                        }
                    }`;

    const variables = {
        where: {
            token: null,
            limit: 1,
            asGzip: true,
            where: JSON.stringify(where),
        }
    };

    const result = await postQuery(query, variables);

    let data;
    if (variables.where.asGzip) {
        data = await utils.ungzip(result.data.getLeadList.dataGzip);
        data = JSON.parse(data);
    } else {
        data = result.data.getLeadList.data;
    }

    return data ?? [];
}

/**
 * Gets a lead
 *
 * @param leadId    string  the Lead's ID
 * @returns {Promise<*>}
 */
export async function getLead(leadId) {
    const query = `query GetLead(
                            $where:LeadsFilterInput!
                        ) { 
                        getLead(where:$where) { 
                            success
                            message
                            ownerId
                            leadId
                            data {
                                projectId
                                leadId
                                leadStatus
                                leadDataBasic { 
                                    name
                                    phone
                                    email
                                }
                                lastWonDataId
                                WDFData
                            }
                            requestedBy
                        }
                    }`;

    const variables = {
        where: {
            leadId: leadId
        }
    };

    const result = await postQuery(query, variables);

    if (!result.data.getLead.success) {
        throw new Error(result.data.getLead.message);
    }

    return result.data?.getLead?.data;

}

/**
 * Get Won Screen Template
 *
 * @param versionId string  the version ID of the template; leave null to get the latest template
 * @returns {Promise<{fields: *[], version}>}
 */
export async function getWonScreenTemplate(versionId = null) {
    const query = `query getForm(
                        $type: FormTypeENUM!,
                        $version: String,
                        $asGzip: Boolean
                    ) {
                  getForm(type: $type, version: $version, asGzip: $asGzip) {
                        version
                        name
                        description
                        fields
                        dataGzip
                  }
              }`;

    const variables = {
        type: 'win',
        version: versionId,
        asGzip: true
    }

    const result = await postQuery(query, variables);

    let data;
    if (variables.asGzip) {
        data = await utils.ungzip(result.data.getForm.dataGzip);
        data = JSON.parse(data);
    } else {
        data = result.data.getForm;
    }

    // this is a legacy field kept for backwards-compatibility,
    // ignore it, and use data.fields instead
    delete data.template;

    let fields = [];
    if (data.fields) {
        if (data.fields === 'string') {
            data.fields = JSON.parse(data.fields);
        }

        // remove deleted fields
        fields = data.fields.filter(x => {
            return !x.isDeleted;
        });
    }
    return {
        version: data.version,
        fields: fields,
    };
}

/**
 * Gets the lead action template
 *
 * @returns {Promise<null|any>}
 */
export async function getLeadActionTemplate() {
    const query = `query GetLeadAndActionTemplate {
                        getLeadAndActionTemplate {
                        description
                        name
                        status
                        template
                    }
                }`;

    const result = await postQuery(query);
    if (result.data?.getLeadAndActionTemplate && result.data.getLeadAndActionTemplate.length > 0) {
        return JSON.parse(result.data.getLeadAndActionTemplate[0].template);
    }
    return null;
}

/**
 * Updates the status of the lead to won status
 *
 * @param leadId
 * @param statusUpdate
 * @param winForm
 *
 * @returns {Promise<unknown>}
 */
export async function winLead(leadId, statusUpdate, winForm) {
    const query = `mutation WinLead($input: WinLeadInput) {
                    winLead(input: $input) {
                    success
                    message
                    ownerId
                    leadId
                    data {
                        leadId
                        lastNote
                        leadStatus
                        lastAction
                        lastStatusUpdate
                      }
                    }
                }
            `;

    const variables = {
        input: {
            leadId: leadId,
            logId: "",
            action: "{\"type\":\"changeStatus\"}",
            statusUpdate: JSON.stringify(statusUpdate),
            wonData: JSON.stringify(winForm)
        }
    }
    const result = await postQuery(query, variables);
    if (!result.data.winLead.success) {
        throw new Error(result.data.winLead.message);
    }
    return result.data.winLead;
}

/**
 * Executes the GraphQL query
 *
 * @param query
 * @param variables
 * @returns {Promise<unknown>}
 */
async function postQuery(query = '', variables = {}) {

    const params = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            "jwttoken": JWT
        },
        body: JSON.stringify({
            query: query,
            variables: variables,
        }),
    }
    console.log('Calling GraphQL', API_URL, params);
    const response = await fetch(API_URL, params);
    const result = await response.json();
    if (result.errors) {
        throw new Error(result.errors[0].message);
    }
    return result;
}
