import * as zlib from "zlib";

/**
 * Decompresses a gzipped BASE64 string
 *
 * @param data
 * @returns {Promise<unknown>}
 */
export function ungzip(data) {
    return new Promise((resolve, reject) => {
        const buffer = Buffer.from(data, 'base64');
        zlib.gunzip(buffer, (error, result) => {
            if (error) {
                reject(error);
            }
            result = result.toString('utf8');
            resolve(result);
        });
    });
}

/**
 * Generates the Lead's URL for inspection
 *
 * @param leadId
 * @returns {string}
 */
export function generateLeadUrl(leadId) {
    // parse environment from Graph API url
    const url = process.env.API_URL;
    const env = url.split('.')[1];
    return `https://manager.${env}.salescandy.com/leads/view/${encodeURIComponent(leadId)}`;
}
