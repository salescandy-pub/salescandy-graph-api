# SalesCandy Graph API Sample Application: Winning a Lead

## What This Script Does

This script illustrates how to use SalesCandy Graph API to update a lead in SalesCandy to "Won" status.

A Lead must always be provided with an Action Status and Won Data Form when it is set to the "Won" state.

This script does the following when it is executed:

1. It gets a lead which is in a 'follow-up', 'dropped', or 'new' state from your account.
   (Note: it will get a list of leads and just pick the first one to use. You should create a test project and specify
   the `PROJECT_ID` in the `.env` file if you are working with a live account)
2. It gets the Win Data Form Template,  inspects the template, and uses the `faker` library to generate fake data to be
   used for the form
3. It gets the Action Status Template,  inspects the template, selects an action ID that is valid to be used for the
   status update, then generates a fake status update
4. It updates the Lead from step (1) with the forms generated in (2) and (3)
5. It gets the latest lead data which should show that the lead was updated to "won" state and provides a link to check

## Setup

1. You will need Node.js 14 or above, with npm. This script was tested on Node.js 14.16.0
2. Install dependencies:

```shell
$ npm install 
```

3. You will need Super Manager access to SalesCandy Manager Portal in order to create your API key to use for this
   script. Create your API key from SalesCandy Manager Portal, under `Management` > `Integrations` > `Graph API` (create
   a read & write key).
4. (Optional) Create a new Project and create some Leads under that project.
5. Copy `.env_sample` file to a file named `.env`. This file contains the environment variables to run this script.
5. Replace the line `YOUR_JWT_TOKEN_HERE` with the token string you created in the previous step.
6. (Optional) put the version ID of the template you want to use. If you don't know the version ID, leave it empty, and
   you can get the version when you run the script the first time, from viewing the console log.

![The version is output in the console when run](./docs/getting%20template%20version%20id.png)

If you leave this value empty, you will always get the latest version of the form template, which may change in the
future and can break your code.

You should use the static version of the template to lock the template you want to use with your code.

8. (Optional) put the project ID that you want to fetch leads from for the script to use. Leave it empty if you want the
   script to get leads from any project. To get the project ID for a Lead, you can inspect the Lead's ID, for example,
   if the Lead ID is `T:257#L|P:843#L:1627542580079%QiTTa`, then the project ID is `843`.

The `.env` file will look something like this:

```dotenv
API_URL = https://graph.v3.salescandy.com
JWT = eyJhb...YXQiOj
WON_SCREEN_TEMPLATE_VERSION_ID = 1643905167506
PROJECT_ID = 843
```

9. You can now run the script with your IDE or from CLI:

```shell
$ node app.js
```

## Graph API Documentation

Refer to the documentation at

1. https://docs.graph.salescandy.com
2. https://support.salescandy.com/hc/en-us/articles/360039313872-SalesCandy-Graph-API

## Contact Support

For any assistance, please contact our Customer Success Team at support@salescandy.com
