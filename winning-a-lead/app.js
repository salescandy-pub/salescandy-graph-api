import * as graphAPI from './lib/graphAPI.js';
import {faker} from '@faker-js/faker';
import * as utils from './lib/utils.js';

// specify a template version or null for latest version
// it is recommended to specify a version to "lock" front-end forms to a specific template
const WON_SCREEN_TEMPLATE_VERSION_ID = process.env.WON_SCREEN_TEMPLATE_VERSION_ID ?? null;

// specify a project ID if you are working in a live account
// and want to test using leads in a single project
const PROJECT_ID = process.env.PROJECT_ID ?? null;

main().then(result => {
    console.log('------------------------------------');
    console.log('RESULT', result);
    console.log('Link:', utils.generateLeadUrl(result.leadId));
    console.log('------------------------------------');
}).catch(err => {
    console.error(err.message, err.stack);
});

/**
 * Illustrates how to update a Lead from SalesCandy to 'won' status using the Graph API
 *
 * @returns {Promise<*>}
 */
async function main() {

    // Step 1: Get a lead
    const lead = await pickRandomLead();

    if (!lead) {
        throw new Error('No valid leads');
    }

    console.log('lead', lead);
    const {leadId, leadStatus} = lead;
    if (!['followUp', 'drop', 'new'].includes(leadStatus)) {
        throw new Error('Lead must not be in won state');
    }

    // Step 2: generate won data form
    const wonData = await generateFakeWinData();
    console.log('wonData', wonData);

    // Step 3: generate action status update form
    const statusUpdate = await generateStatusUpdate();
    console.log('statusUpdate', statusUpdate);

    // Step 4: update the lead
    const winLeadResponse = await graphAPI.winLead(leadId, statusUpdate, wonData);
    console.log('winLeadResponse', winLeadResponse);

    // Step 5: get the updated lead to confirm update
    const leadAfter = await graphAPI.getLead(leadId);

    if (leadAfter.leadStatus === 'won') {
        console.log('Successfully updated lead to won status');
    } else {
        console.log('Failed to update lead to won status');
    }

    return leadAfter;
}

/**
 * Picks a lead with follow-up, dropped, or new  status from the leadlist
 *
 * @returns {Promise<null|*>}
 */
async function pickRandomLead() {
    let params = {
        leadStatus: {
            in: ['followUp', 'drop', 'new']
        }
    }
    if (PROJECT_ID) {
        params = {
            ...params, ...{
                projectId: {
                    eq: PROJECT_ID
                }
            }
        }
    }

    const leads = await graphAPI.getLeadList(params);

    if (leads.length > 0) {
        return leads[0];
    }
    return null;
}

/**
 * Generates a won data form according to a template
 *
 * @returns {Promise<{data: {}, version}>}
 */
async function generateFakeWinData() {
    const wonScreenTemplate = await graphAPI.getWonScreenTemplate(WON_SCREEN_TEMPLATE_VERSION_ID, true);
    console.log('wonScreenTemplate version', wonScreenTemplate.version);
    console.table(wonScreenTemplate.fields);

    const nestedFormDataLength = 3;
    const checkboxDataLength = 10;

    return {
        version: wonScreenTemplate.version,
        data: generateData(wonScreenTemplate.fields),
    }

    /**
     * Generates a win data object using faker
     *
     * @param fields    array The fields from the template
     * @returns {{}}
     */
    function generateData(fields) {
        const result = {};


        // note: all currently possible field types are shown below:
        //       in the future, we will be adding more field types, corresponding to
        //       HTML input types: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input
        for (const field of fields){
            let value = null;
            switch (field.fieldType) {
                case 'singleline':
                    // this field should be presented as a single line text input (text)
                    value = faker.lorem.words(3);
                    break;

                case 'multiline':
                    // this field should be presented as a multi-line text input (textarea)
                    value = faker.lorem.paragraphs(2);
                    break;

                case 'number':
                    // this field should be presented as a number input
                    // with data type integer or float to specify the number type
                    if (field.dataType === 'integer') {
                        value = faker.datatype.number();
                    }
                    if (field.dataType === 'float') {
                        value = faker.datatype.float();
                    }
                    break;

                case 'email':
                    // this field should be presented as an email input
                    value = faker.internet.email();
                    break;

                case 'phone':
                    // this field should be presented as a phone number input
                    value = faker.phone.phoneNumber();
                    break;

                case 'float':
                    // this field should be presented as a decimal input
                    // to accept float numbers
                    value = faker.datatype.float();
                    break;

                case 'integer':
                    // this field should be presented as a integer input
                    // to accept integer numbers
                    value = faker.datatype.number();
                    break;

                case 'dropdown':
                    // this field should be presented as a dropdown
                    // with the field.options as the dropdown
                    // all values from dropdowns are treated as strings
                    value = field.options[0].name;
                    break;

                case 'checkbox':
                    // this field should be presented as checkboxes
                    // with the field.options as the options
                    // all values from checkboxes are treated as strings
                    let maxSelected = field.options.length;
                    let minSelected = 1;
                    for (const rule of field.validationRules){
                        if (rule.rule === 'minSelected') {
                            minSelected = rule.value;
                        }
                        if (rule.rule === 'maxSelected') {
                            maxSelected = rule.value;
                        }
                    }
                    value = [];
                    for (let i = 0;
                         i < checkboxDataLength && i < field.options.length && i < maxSelected;
                         i++){
                        value.push(field.options[i].name);
                    }
                    break;
                case 'nestedForm':
                    // a nested form is an array of forms, comprised of other field types
                    // example use case: order form for multiple items
                    // here we create a few items to illustrate the usage
                    value = [];
                    for (let i = 0; i < nestedFormDataLength; i++){
                        value.push(generateData(field.fields));
                    }
                    break;

                default:
                    // throw new Error(`Unhandled fieldType ${field.fieldType}`);
                    break;

            }

            // conform to validation rules:
            // @note all currently used rules are shown here:
            for (const rule of field.validationRules){

                switch (rule.rule) {
                    case 'maxValue':
                        // maximum number value (enforced for number data types)
                        if (value > rule.value) {
                            value = rule.value;
                        }
                        break;

                    case 'minValue':
                        // minimum number value (enforced for number data types)
                        if (value < rule.value) {
                            value = rule.value;
                        }
                        break;

                    case 'maxSelected':
                        // maximum number of options to select (enforced for checkbox field types)
                        if (value.length > rule.value) {
                            throw new Error('Selected more than the allowed number of options');
                        }
                        break;

                    case 'minSelected':
                        // minimum number of options to select (enforced for checkbox field types)
                        if (value.length < rule.value) {
                            throw new Error('Selected less than the allowed number of options');
                        }
                        break;

                    case 'maxLength':
                        // minimum character length for the value (enforced for string data types)
                        value = value.substring(0, rule.value);
                        break;

                    case 'minLength':
                        // minimum character length for the value (enforced for string data types)
                        if (value.length < rule.value) {
                            throw new Error('value does not satisfy minimum length');
                        }
                        break;

                    case 'isRequired':
                        // the field is required and must have a value
                        if (value == null || value === '') {
                            throw new Error('value is required');
                        }
                        break;

                    case 'allowInvalid':
                        // used internally, it does not enforce strict validation
                        // on lead creation, but will force the salesperson to correct the value
                        break;

                    default:
                        break;
                }
            }

            result[field.fieldId] = value;
        }
        return result;
    }
}

/**
 * Generates a status update form
 * @returns {Promise<{leadStatus: string, notes: string, actionId: *}>}
 */
async function generateStatusUpdate() {
    const leadActionTemplate = await graphAPI.getLeadActionTemplate();
    console.table(leadActionTemplate);

    const action = findAValidActionForWin();
    const actionId = action.templateId;
    console.log(actionId, action);

    return {
        actionId: actionId,
        notes: faker.lorem.sentence(),
        leadStatus: 'won'
    }

    /**
     *  Here we are looking for the first action that is valid to be used
     *  for 'won' status updates by the Graph API
     *
     *  1. It can be used for 'won' actions
     *  2. It is not deleted
     *  3. It must be a "tag" action (i.e. not a phone call, sms, etc.)
     */
    function findAValidActionForWin() {
        for (const action of leadActionTemplate){
            if (action.won && !action.isDeleted && action.tag) {
                return action;
            }
        }
    }
}
